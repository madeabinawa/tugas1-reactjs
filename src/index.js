import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';

// Membuat variable constanta
const nama = "Made"
const tahun = 2020
const tahun_lahir = 1997

// Membuat element react dengan create Element
const element_1 = React.createElement('span', { className: 'warna' }, 'Selamat Pagi')

// Membuat element dengan sintaksis jsx
const element_2 = (
  <div>
    <h1 className="warna">Halo {nama}, {element_1}</h1>
    <h2 className="warna">Umur saya saat ini {tahun - tahun_lahir}</h2>
    <h2 className="warna">Umur saya tahun depan {tahun - tahun_lahir +1}</h2>
    <br />
  </div>
)

ReactDOM.render(
  element_2,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
